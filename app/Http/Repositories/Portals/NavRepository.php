<?php

namespace App\Http\Repositories\Portals;

class NavRepository
{
    public static function getList() {
        return $navs = [
            ['title' => 'Product', 'route' => '#'],
            ['title' => 'Features', 'route' => '#'],
            ['title' => 'Marketplace', 'route' => '#'],
            ['title' => 'Company', 'route' => '#'],
            ['title' => 'Login', 'route' => '#', 'is_highlight' => 1],
        ];
    }
}
