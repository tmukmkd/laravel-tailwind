<?php

namespace App\Http\Controllers;

use App\Helpers\TMTBHelpers;

class MoviesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $popularMovies = TMTBHelpers::getMovies('popular_movies');

        $nowPlayingMovies = TMTBHelpers::getMovies('now_playing_movies', 5);

        $genres = TMTBHelpers::getMovieGenres();

        return view('index', compact('popularMovies', 'genres', 'nowPlayingMovies'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $movie = TMTBHelpers::getMovie($id);

        $credits = TMTBHelpers::getMovieCredits($id);

        $casts = $credits['cast'];

        return view('show', compact('movie', 'casts'));
    }
}
