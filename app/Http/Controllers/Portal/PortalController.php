<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\Controller;
use App\Http\Repositories\Portals\NavRepository;

class PortalController extends Controller
{
    public function __invoke()
    {
        $navs = NavRepository::getList();

        return view('portals.index', compact('navs'));
    }
}
