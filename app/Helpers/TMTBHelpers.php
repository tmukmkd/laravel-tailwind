<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

class TMTBHelpers
{
    /**
     * @param string $type
     * @param int $chunk
     * @return mixed
     */
    public static function getMovies($type = 'popular_movies', $chunk = 10)
    {
        switch ($type) {
            case 'now_playing_movies':
                return self::getNowPlayingMovies($chunk);
                break;
            case 'popular_movies':
            default:
                return self::getPopularMovies($chunk);
                break;
        }
    }

    /**
     * @param int $chunk
     * @return mixed
     */
    protected static function getPopularMovies($chunk = 10)
    {
        $popularMovies = Http::withToken(config('services.tmtb.token'))
            ->get(config('services.tmtb.url') . 'movie/popular')
            ->json()['results'];

        return collect($popularMovies)->chunk($chunk)[0];
    }

    protected static function getNowPlayingMovies($chunk = 10)
    {
        $nowPlayingMovies = Http::withToken(config('services.tmtb.token'))
            ->get(config('services.tmtb.url') . 'movie/now_playing')
            ->json()['results'];

        return collect($nowPlayingMovies)->chunk($chunk)[0];
    }

    public static function getMovieCredits($id)
    {
        return Http::withToken(config('services.tmtb.token'))
            ->get(config('services.tmtb.url') . 'movie/' . $id . '/credits')
            ->json();
    }

    public static function getMovie($id)
    {
        return Http::withToken(config('services.tmtb.token'))
            ->get(config('services.tmtb.url') . 'movie/' . $id)
            ->json();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public static function getMovieGenres()
    {
        $movieGenres = Cache::remember('movie_genres', 3600, function () {
            return Http::withToken(config('services.tmtb.token'))
                ->get(config('services.tmtb.url') . 'genre/movie/list')
                ->json()['genres'];
        });

        return collect($movieGenres)->mapWithKeys(function ($genre) {
            return [$genre['id'] => $genre['name']];
        });
    }
}
