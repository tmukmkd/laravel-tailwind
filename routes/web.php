<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'Portal\PortalController');

//Route::resource('movies', 'MoviesController')->names('movies');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
