@extends('layouts.app')

@section('content')
    <div class="movie-info border-b border-gray-800">
        <div class="container mx-auto px-4 py-16 flex">
            <div class="w-96">
                <img src="{{'https://image.tmdb.org/t/p/w500' . $movie['poster_path']}}"
                     alt="{{Str::snake($movie['original_title'])}}">
            </div>
            <div class="ml-24">
                <h2 class="text-4xl font-semibold">{{$movie['original_title']}}</h2>
                <div class="flex items-center text-gray-400 text-sm">
                    <span class="mr-1"><i class="fa fa-star text-orange-500"></i></span>
                    <span>{{$movie['vote_average'] * 10}}%</span>
                    <span class="mx-2">|</span>
                    <span>{{\Carbon\Carbon::parse($movie['release_date'])->format('d-m-Y')}}</span>
                    <span class="mx-2">|</span>
                    <span>
                        @foreach($movie['genres'] as $genre)
                            {{ $genre['name'] . (!$loop->last ? ', ' : '') }}
                        @endforeach
                    </span>
                </div>
                <p class="text-gray-300 mt-8">{{$movie['overview']}}</p>
                <div class="mt-12">
                    <div class="text-white font-semibold">Featured Cast</div>
                    <div class="flex mt-4">
                        <div>
                            <div>Quentin Tarantino</div>
                            <div class="text-sm text-gray-400">Director, Screenplay</div>
                        </div>
                        <div class="ml-8">
                            <div>Quentin Tarantino</div>
                            <div class="text-sm text-gray-400">Director, Screenplay</div>
                        </div>
                    </div>
                </div>
                <div class="mt-12">
                    <button
                        class="flex items-center bg-orange-500 text-gray-900 font-semibold px-5 py-5 hover:bg-orange-600 transition ease-in-out duration-150 rounded">
                        <svg class="h-8 w-8" version="1.1" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M371.7 238l-176-107c-15.8-8.8-35.7 2.5-35.7 21v208c0 18.4 19.8 29.8 35.7 21l176-101c16.4-9.1 16.4-32.8 0-42Zm132.3 18c0-137-111-248-248-248 -137 0-248 111-248 248 0 137 111 248 248 248 137 0 248-111 248-248Zm-448 0c0-110.5 89.5-200 200-200 110.5 0 200 89.5 200 200 0 110.5-89.5 200-200 200 -110.5 0-200-89.5-200-200Z"
                                fill="#000"></path>
                        </svg>
                        <span class="ml-2">Play Trailer</span>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="movie-cast border-b border-gray-800">
        <div class="container mx-auto px-4 py-16">
            <h2 class="text-4xl font-semibold">Cast</h2>
            <div class="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-5 gap-8">
                <div class="mt-8">
                    <a href="#">
                        <img src="{{asset('images/movie_covers/the_blacklist.jpg')}}"
                             alt="the_blacklist"
                             class="hover:opacity-25">
                    </a>
                    <div class="mt-2">
                        <div class="flex items-center mt-1 text-gray-400 text-sm">
                            <span>Realname</span>
                        </div>
                        <div class="flex items-center text-gray-400 text-sm">
                            John Doe
                        </div>
                    </div>
                </div>
                <div class="mt-8">
                    <a href="#">
                        <img src="{{asset('images/movie_covers/inception.jpg')}}" alt="inception"
                             class="hover:opacity-25">
                    </a>
                    <div class="mt-2">
                        <div class="flex items-center mt-1 text-gray-400 text-sm">
                            <span>Realname</span>
                        </div>
                        <div class="flex items-center text-gray-400 text-sm">
                            John Doe
                        </div>
                    </div>
                </div>
                <div class="mt-8">
                    <a href="#">
                        <img src="{{asset('images/movie_covers/the_flash.jpg')}}" alt="the_flash"
                             class="hover:opacity-25">
                    </a>
                    <div class="mt-2">
                        <div class="flex items-center mt-1 text-gray-400 text-sm">
                            <span>Realname</span>
                        </div>
                        <div class="flex items-center text-gray-400 text-sm">
                            John Doe
                        </div>
                    </div>
                </div>
                <div class="mt-8">
                    <a href="#">
                        <img src="{{asset('images/movie_covers/the_blacklist.jpg')}}"
                             alt="the_blacklist"
                             class="hover:opacity-25">
                    </a>
                    <div class="mt-2">
                        <div class="flex items-center mt-1 text-gray-400 text-sm">
                            <span>Realname</span>
                        </div>
                        <div class="flex items-center text-gray-400 text-sm">
                            John Doe
                        </div>
                    </div>
                </div>
                <div class="mt-8">
                    <a href="#">
                        <img src="{{asset('images/movie_covers/inception.jpg')}}" alt="inception"
                             class="hover:opacity-25">
                    </a>
                    <div class="mt-2">
                        <div class="flex items-center mt-1 text-gray-400 text-sm">
                            <span>Realname</span>
                        </div>
                        <div class="flex items-center text-gray-400 text-sm">
                            John Doe
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
