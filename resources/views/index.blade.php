@extends('layouts.app')

@section('content')
    <div class="container mx-auto px-4 pt-16">
        <div class="popular_movies">
            <h2 class="uppercase text-orange-500 weight font-semibold tracking-wider text-lg">popular movies</h2>
            <div class="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-5 gap-8">
                @foreach($popularMovies as $movie)
                    <x-movie-card :movie="$movie" :genres="$genres"/>
                @endforeach
            </div>
        </div>

        <div class="now_playing py-20">
            <h2 class="uppercase text-orange-500 weight font-semibold tracking-wider text-lg">now playing</h2>
            <div class="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-5 gap-8">
                @foreach($nowPlayingMovies as $movie)
                    <x-movie-card :movie="$movie" :genres="$genres"/>
                @endforeach
            </div>
        </div>
    </div>
    <div class="container mx-auto flex px-4 py-12">
        <ul class="flex items-center">
            <li class="ml-16">
            </li>
        </ul>
    </div>
@endsection
