@extends('layouts.clean')

@section('content')
    <div class="container mx-auto px-4 h-full">
        <div class="flex content-center items-center justify-center h-full">
            <div class="w-full lg:w-4/12 px-4">
                <div
                    class="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-gray-300 border-0"
                >
                    <div class="flex-auto px-4 lg:px-10 py-10 pt-10">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="relative w-full mb-3">
                                <label
                                    class="block uppercase text-gray-700 text-xs font-bold mb-2"
                                    for="grid-password"
                                    autofocus
                                >{{ __('E-Mail Address') }}</label>
                                <input
                                    type="email"
                                    required
                                    class="px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:shadow-outline w-full @error('email') border border-red-500 @enderror"
                                    placeholder="Email"
                                    style="transition: all 0.15s ease 0s;"
                                />

                                @error('email')
                                <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="relative w-full mb-3">
                                <label
                                    class="block uppercase text-gray-700 text-xs font-bold mb-2"
                                    for="grid-password"
                                >{{ __('Password') }}</label>
                                <input
                                    type="password"
                                    required
                                    class="px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:shadow-outline w-full @error('password') border border-red-500 @enderror"
                                    placeholder="Password"
                                    style="transition: all 0.15s ease 0s;"
                                    autocomplete="current-password"
                                />

                                @error('password')
                                <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                @enderror
                            </div>

                            <div>
                                <label class="inline-flex items-center cursor-pointer"
                                ><input
                                        id="remember"
                                        type="checkbox"
                                        class="form-checkbox text-gray-800 ml-1 w-5 h-5"
                                        name="remember"
                                        style="transition: all 0.15s ease 0s;"
                                    /><span class="ml-2 text-sm font-semibold text-gray-700"
                                    >Remember me</span
                                    ></label
                                >
                            </div>

                            <div class="text-center mt-6">
                                <button
                                    class="bg-gray-900 text-white active:bg-gray-700 text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full"
                                    style="transition: all 0.15s ease 0s;"
                                >
                                    {{ __('Login') }}
                                </button>
                            </div>
                        </form>
                        <hr class="mt-6 border-b-1 border-gray-400"/>
                        <div class="rounded-t mb-0 px-6 pt-6">
                            <div class="btn-wrapper text-center">
                                <button
                                    class="bg-white active:bg-gray-100 text-gray-800 font-normal px-4 py-2 rounded outline-none focus:outline-none mr-2 mb-1 uppercase shadow hover:shadow-md inline-flex items-center font-bold text-xs"
                                    type="button"
                                    style="transition: all 0.15s ease 0s;"
                                >Forget Password
                                </button>
                                <button
                                    class="bg-white active:bg-gray-100 text-gray-800 font-normal px-4 py-2 rounded outline-none focus:outline-none mr-1 mb-1 uppercase shadow hover:shadow-md inline-flex items-center font-bold text-xs"
                                    type="button"
                                    style="transition: all 0.15s ease 0s;"
                                >Sign In
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
