<div class="mt-8 flex flex-col">
    <a href="/movies/{{$movie['id']}}">
        <img src="{{'https://image.tmdb.org/t/p/w500' . $movie['poster_path']}}"
             alt="{{Str::snake($movie['original_title'])}}"
             class="hover:opacity-25">
    </a>
    <div class="mt-2 flex-grow">
        <a href="/movies/{{$movie['id']}}" class="text-lg mt-2 hover:text-gray-300">{{$movie['original_title']}}</a>
        <div class="flex items-center mt-1 text-gray-400 text-sm">
            <span class="mr-1"><i class="fa fa-star text-orange-500"></i></span>
            <span>{{$movie['vote_average'] * 10}}%</span>
            <span class="mx-2">|</span>
            <span>{{\Carbon\Carbon::parse($movie['release_date'])->format('d-m-Y')}}</span>
        </div>
    </div>
    <div class="bg-yellow-400 flex items-end text-gray-400 text-sm">
        @foreach($movie['genre_ids'] as $genre)
            {{ $genres->get($genre) . (!$loop->last ? ', ' : '') }}
        @endforeach
    </div>
</div>
