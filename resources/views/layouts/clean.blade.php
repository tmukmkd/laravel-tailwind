<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Laravel</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" href="{{asset('img/favicon.png')}}"/>
    <link
        rel="apple-touch-icon"
        sizes="76x76"
        href="{{asset('img/apple-icon.png')}}"
    />

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    {{--    <script src="{{ asset('js/manifest.js') }}" defer></script>--}}
    {{--    <script src="{{ asset('js/vendor.js') }}" defer></script>--}}
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script defer src="/js/all.js"></script> <!--load all styles -->

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="text-gray-800 antialiased">
@include('layouts.portal-nav')
<main>
    <section class="absolute w-full h-full">
        <div class="absolute top-0 w-full h-full bg-gray-900"></div>
        @yield('content')
        @include('layouts.clean-footer')
    </section>
</main>
</body>
</html>
